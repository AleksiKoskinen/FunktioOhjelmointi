(ns tehtavatprojekti.core
  (:gen-class))

(defn -main
  "I don't do a whole lot ... yet."
  [& args]
  (println "Hello, World!"))


;Tehtava2 below

(println (+ 4(* 2 5)))
(println (+ 1 2 3 4 5))

(defn hello ; function nimi
  "Gives out personalized greetings." ; valinnainen dokumentaatio
  [name] ; parametrit
  (str "Hello, " name "!"))

((fn [name] (str "Tervetuloa tylypahkaan, " name "!")) "Aleksi" ) ;type to repl, prints as asked

(println (:middle {:first "Urho" :middle "Kaleva" :last "Kekkonen"}))

;Tehtava4 below

(def square ;funktion nimi
  (fn [number] (* number number))) ;kutsutaan uusi anonyymi funktio johon laitetaan syötetty numero, ja palautetaan sen square arvo

;Tehtava5 below

(defn karkausvuosi? [vuosi]
  (if-not (= (mod vuosi 4) 0) ;Katsoo onko moduluksen tulos 0, eli onko jaollinen 4'llä
    false ;false jos ei ole jaollinen
    (if (= (mod vuosi 400) 0) ; Jos on jaollinen, niin katsotaan onko se jaollinen 400'lla
      true  ;True jos on
      (if (= (mod vuosi 100) 0); jos ei ole jaollinen 400'lla, niin katsotaan onko jaollinen 100'lla
        false ;Jos on , niin laitetaan false. Eli tällöin jaollinen 100'lla, jolloin false, mutta jos 400 niin laitetaan true
        true) ;Palataan ekaan ehtoon, eli jos on jaollinen 4'llä , niin laitetaan true
          ))) ;Sulku kloussaukset
