(ns projektit.core
  (:gen-class))

;Tehtava1

(defn tehtava1  []
  (println "Anna luku:") ;kysytään luku
  (let [luku (read-line)] ;otetaan se 'muuttujaan' talteen
        (if (>  (Integer. luku) 0) ;Katsotaan onko se suurempi kuin 0, tehdään integer muunnos samalla
          (if (= (mod (Integer. luku) 2) 0) ;´katsotaan moduluksen tulos, eli jos se on 0, niin se on parillinen luku
            (println (str "Luku " luku " on parillinen" )) ;jos on parillinen
            (println (str "Luku " luku " ei ole parillinen" )) ;jos ei ole (else haara)
          )
            (println (str "Syötä luku joka on suurempi kuin 0 !")) ;ylimmän if kutsun -else haara tässä, eli tähän mennään jos luku on <= 0
        )
  )
)
;tehtava2

(defn tehtava2  []
  (println "Anna luku:")
  (let [luku (read-line)]
        (if (>  (Integer. luku) 0)
          (if (= (mod (Integer. luku) 2) 0)
            (println (str "Luku " luku " on parillinen" ))
            (println (str "Luku " luku " ei ole parillinen" ))
          )
            (do ;tässä else haarassa käytetään do kutsua, jolloin
              (println "Syötä luku joka on suurempi kuin 0 !") ;printataan tuloste että luku on väärä
              (recur )) ;ja kutsutaan rekursiolla funktiota uudestaan. Parametreja ei ollut joten jätetään se tyhjäksi 
            
        )
  )
)
;Tehtava3

(defn tehtava3  [ylaraja] ; otetaan parametrina yläraja
  (loop [acc 1] ;otetaan muuttuja acc, johon aina laitetaan uusi numero. Alustus on 1
        (if (<  acc ylaraja) ;Katsotaan onko luku suurempi kuin annettu yläraja
          (if (= (mod acc 3) 0) ;Katsotaan onko moduluksen tulos 0, jos on nii se on jaollinen kolmosella
            (do ;do haara käytetään jotta saadaan usea homma tehtyä
              (println (str "Luku " acc " on jaollinen kolmella" )) ;Tulostetaan luku
              (recur (inc acc)) ;Kutsutaan looppia uudestaan, tehdään uusi acc joka on yhden suurempi
            )
            (recur (inc acc)) ;Jos ei ollu jaollinen kolmosella, niin tehdään sama temppu kuin yllä, mutta ei tulosteta numeroa
          )
            (println "Siinä ole kaikki jaolliset luvut! The End"); else haara, eli jos acc on yhtä suuri kuin yläraja. 
        )
  )
)
;Tehtava4

(defn tehtava4 [] ; tehdään funktio
  (loop [lottery (set [])] ;käytetään loop rakennetta, parametrina tyhjä set muuttuja
        (if (< (count lottery) 7) ;jos setin koko on alle 7
            ; kutsutaan looppia uudestaan, ja tehdään uusi set taulukko missä on arvottu
            ; random numero välillä 0-38, Huoma koska se arpoo luvun 0-38 niin täytyy
            ; sanoa inc arvottuun numeroon, silloin saadaan luku väliltä 1-39
            ; conj lisää lottery settiin tämän uuden arvotun luvun
            (recur (conj lottery (inc (rand-int 39)))) 
            (print (seq lottery)) ;else-haara, eli jos setin koko on 7, tulostetaan se seq käskyllä
        ) ;HUOM! Koska on set-rakenne, niin se ei hyväksy duplikaatteja. Tällöin loop
  )       ;arpoo uusia numeroja niin kauan, että koko saadaan seiskaan, ja duplikaatteja EI ole. 
)

;Tehtava5

(defn tehtava5 [p, q]  ;Funktio, kaksi parametria
  (if (= q 0) ;Katsotaan onko q 0, eli onko yhteinen tekijä löytynyt
      (println p) ;Tulostetaan tämä yhteinen tekijä
      (recur q (mod p q)) ;Jos ei ollu vielä löytynyt yhteistä tekijää, rekursiivisesti kustutaan 
  )                       ;funktiota uudestaan, laitetaan parametrit niin että q menee p'n paikalle
)                         ;ja q paikalle menee p'n ja q'n modulus, eli se jäännös mikä jää moduluksesta 
                          ;(jos 0, laitetaan arvoksi kyseinen suurin yhteinen tekijä)