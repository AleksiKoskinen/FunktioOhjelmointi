import static java.util.Comparator.comparing;
import static java.util.stream.Collectors.toList;

import java.util.Arrays;
import java.util.List;

public class Tehtava2 {

	public static void main(String ...args){   
		Trader raoul = new Trader("Raoul", "Cambridge");
	    Trader mario = new Trader("Mario","Milan");
	    Trader alan = new Trader("Alan","Cambridge");
	    Trader brian = new Trader("Brian","Cambridge");
		
		List<Transaction> transactions = Arrays.asList(
	        new Transaction(brian, 2011, 300), 
	        new Transaction(raoul, 2012, 1000),
	        new Transaction(raoul, 2011, 400),
	        new Transaction(mario, 2014, 710),	
	        new Transaction(mario, 2014, 1700),
	        new Transaction(alan, 2013, 950)
	    );
		
		/*Filteröidään mukaan vain rahansiirrot jotka on tehty 2012 jälkeen
		  ja joiden arvo on yli 900. Sen jälkeen sortataan ne hinnan mukaan,
		  ja sitten laitetaan listaan, eli muutetaan striimi list tyypiksi*/
		List<Transaction> tr = transactions.stream()
                .filter(transaction -> transaction.getYear() > 2012 && transaction.getValue() > 900)
                .sorted(comparing(t -> t.getValue()))
                .collect(toList());
		
		System.out.println(tr);
		
		
		/* Huomattavan hankalaa tehdä seuraava operaatio reducen kanssa, mutta
		 * jotta se on sillä tehty, niin eka tehdään mapilla uusi 'lista' missä
		 * on putkessa kaikki menun eri ruokatyypit. Sitten reducella käydään jokainen tyyppi läpi,
		 * ja aina addType metodilla tuotetaan uusi stringi, missä on lisätty alkion tyyppi
		 */
		String s = Dish.menu.stream()
				.map(dish -> dish.getType().toString())
				.reduce("", (a,b) -> a = addType(b));
		
		System.out.println("Number of types on the list:" + s);
						
	}
	
	private static int FISH = 0,MEAT = 0, OTHER = 0;
	
	public static String addType(String type){
		if(type == "FISH")
			FISH++;
		if(type == "MEAT")
			MEAT++;
		if(type == "OTHER")
			OTHER++;
		
		return "MEAT = "+ MEAT + ". kertaa. FISH = "+FISH+". kertaa. OTHER = "+OTHER+". kertaa.";
	}
}
