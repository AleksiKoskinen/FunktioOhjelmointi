import java.util.stream.Stream;

public class Tehtava3 {
	
	public static void main(String [] args) {
	
		int startNumber = 1 + (int)(Math.random() * ((6 - 1) + 1));
		
		int i = Stream.iterate(startNumber, n -> (int)(Math.random() * 6 + 1 ))
		.limit(20) //Otetaan 20 ekaa
		.filter(number -> number == 6) //Filteröidään vain numero kuutoset mukaan
		//lasketaan kuinka monta alkiota on, eli montako kutosta on. Kasvatetaan siis aina accumulatoria yhdellä joka alkion käynnillä
		.reduce(0, (a,b) -> Integer.sum(a, 1));
		
		System.out.println(i);
	}
}