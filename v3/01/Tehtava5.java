import java.util.*;
import java.util.stream.*;
import java.nio.charset.Charset;
import java.nio.file.*;


public class Tehtava5 {

    public static void main(String...args) throws Exception{
       
         //Kuten esimerkissä, haetaan tiedosto sijainnista, käytetään splittiä erottamaan sanat välilyönnillä
    	 //, flatmapilla viedään jokainen sana mappiin, ja lopuksi tehdään siitä String lista
         List<String> list = Files.lines(Paths.get("C:/Users/Aleksi/Desktop/Java8Funktio/Java8/src/V0301/kalevala.txt"), Charset.defaultCharset())
                                 .flatMap(line -> Arrays.stream(line.split(" ")))
                                 .collect(Collectors.toList());
   
         //Tehdään String, integer pari Map, johon laitetaan listan streemistä ensin itse sana Stringiksi
         //, ja sitten sen arvo laitetaan 1, jos se on jo siellä, niin Integer::sum plussaa vanhan arvon ja ykkösen,
         //eli kasvattaa yhdellä sanan arvoa
         Map <String, Integer> count = list.stream()
             .collect(Collectors.toMap(w -> w, w -> 1, Integer::sum));
  
         System.out.println(count);
        
    }
    
}
