package V0301;

public class Tehtava1 {

	public static void main(String[] args) {

		/*Huomaa alla ei voi olla 5/9, koska se tekee tuloksen silloin automaattisesti
		  integer muotoon, eli pyöristää lähimpään tasalukuun, jolloin sen tulos on aina 0
		  Siksi pitää laittaa 5.0 ja 9.0, jotta kääntäjä osaa laittaa tuloksen double muotoon, 
		  jolloin saadaan oikea tulos
		  */
		
		countCelsiusAndArea count1 = (x) -> (5.0/9.0) * (x - 32); //lambda lause
		countCelsiusAndArea count2 = (x) -> Math.PI * x * x;  //lambda lause
		
		System.out.println(count1.count(100));
		System.out.println(count2.count(1));
		
    }
}

@FunctionalInterface
interface countCelsiusAndArea{
    public abstract double count(double x);
}

