import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class Tehtava4 {

	public static void main(String[] args) {
		
		final int[] taulu1 = {1, 2, 3};
		final int[] taulu2 = {3, 4};
		
		List<int[]> list = IntStream.of(taulu1)  //.parallel()
		        .mapToObj(i -> IntStream.of(taulu2).mapToObj(j -> new int[]{i, j}))
		      /*Tehdään flatmap jolla suljetaan stream. Tämän jälkeen on mahdollista
		        tehdä mapista lista Collectors.toListin avulla
		        */
		        .flatMap(map -> map) 
		        .collect(Collectors.toList());
		
		/*Tehdään String lista, jossa jokaiseen alkioon menee haettu alkion tulos, esim ekaan alkioon 
		  tehään stringi jonka arvot saadaan mapista
		*/
		List<String> result = list.stream().map(map -> "(" + map[0] + "," + map[1] + ")").collect(Collectors.toList());
		
		System.out.println(result); //tulostaa: [(1,3), (1,4), (2,3), (2,4), (3,3), (3,4)]
				
	}
}