package V0302;

import java.util.function.IntSupplier;
import java.util.stream.IntStream;

public class Tehtava4 {

	public static void main(String[] args) {
		
		/*Tehdään sisäluokan ilmentymä fibo, annetaan
		  sille kaksi parametriä ja metodi mikä antaa ilmentymälle arvon
		  */
		IntSupplier fibo = new IntSupplier() {
			private int edellinen = 0;
	        private int nykyinen = 1;

	        public int getAsInt() {
	        	int seuraava = this.edellinen + this.nykyinen;
	        	this.edellinen = this.nykyinen;
	        	this.nykyinen = seuraava;
	        	return this.edellinen;
	        }
	    };
	    
	    //Generoidaan 15 kertaa striimiin aina uusi arvo (fibo ilmentymän arvo), ja tulostetaan näytölle
	    IntStream.generate(fibo).limit(15).forEach(x -> System.out.print(x+", "));
	}	
	
}



