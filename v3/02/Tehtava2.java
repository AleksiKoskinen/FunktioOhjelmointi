package V0302;

import java.util.function.DoubleUnaryOperator;

public class Tehtava2 {
	
    static DoubleUnaryOperator makePistelaskuri(double kPiste, double lisapisteet){
    	
    	//Eli palautetaan lambda funktio jossa x on saadut pisteet
            return (x) -> 60 + ((x - kPiste) * lisapisteet);
    };
        
    public static void main(String[] args) {
       
       DoubleUnaryOperator normaaliLahti = makePistelaskuri(90, 2.0);
       
       /*Käytetään DoubleUnaryOperaaatorin metodia applyAsDouble, joka palauttaa arvon
         doublena. Käytetään currying menetelmää eli annetaan tässä vain yksi parametri funktiolle
       */
       System.out.println(normaaliLahti.applyAsDouble(100)); 
       
    }    
}
