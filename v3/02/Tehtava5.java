package V0302;

import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.function.Function;

public class Tehtava5 {
	
	public static void main(String[] args) {
		
		/*Castataan funktiot pisteeksi. Lambda lauseessa s saa arvon apply metodin parametrista p, joka on
		 * piste olio. Sitten esim makeSiirto palauttaa tuon piste olion, mihin on muutokset tehty. Sitten tuo olio 
		 * siirtyy ketjussa eteenpäin skaalaus funktiolle, eli siinä taas s saa tuon saman Piste olion parametriksi etc, 
		 * kunnes päästään ketjun loppuun ja saadaan lopputulos.
		 */
		   Function<Piste,Piste> siirto = s -> Piste.makeSiirto(1, 2, s);
	       Function<Piste,Piste> skaalaus = s -> Piste.makeSkaalaus(2,s);
	       Function<Piste,Piste> kierto = s -> Piste.makeKierto(Math.PI,s);
	       Function<Piste,Piste> muunnos = siirto.andThen(skaalaus).andThen(kierto);
	       
	       //Tehdään Piste oliot ja tyhjä listä
	       Piste[] pisteet = {new Piste(1,1), new Piste(2,2), new Piste(3,3)};
	       List<Piste> uudetPisteet = new CopyOnWriteArrayList<Piste>();
	       
	       /*Käydään kaikki 3 pistettä läpi, ja tehdään jokaiselle
	        * muunnos applu metodilla. Siihen laitetaan parametriksi läpikäytävä Piste olio
	        */
	       for (Piste p: pisteet){
	           uudetPisteet.add((Piste)muunnos.apply(p));
	       } 
	       	//Tulostetaan jokaisesta Piste oliosta x ja y
	       uudetPisteet.forEach(p -> System.out.println(p.getX() +","+p.getY()));
	    }	
	
}

class Piste{
	
	private double x,y;
	
	Piste(double x, double y){		
		this.x = x;
		this.y = y;
	}
	/*Kyllä sai miettiä että sai ratkaisun. MUtta siis, alla oleva metodi pitää olla staattinen
	 * jotta pääohjelman funktio lauseke pääsee siihen käsiksi. NOH ylläolevat x ja y arvot Eivät taaseen
	 * voi olla staattisia, koska ne pitää olla niillä 3 luomallamme piste oliolla jokaisella omat arvot, eli ei saa käyttää 
	 * samaa staattista arvoa. Jotta x ja y'hyn pääsee käsiksi alla olevassa metodissa, luotiin getX ja getY 
	 * metodit, jolla voi hakea olio instanssin omat x ja y arvot, joita sitten muutellaan sopiviksi.
	 * Palautetaan funktiolle muuteltu Piste olio.
	 */
	public static <T, R> Piste makeSiirto(double a, double b, Piste p){
		
		double x4 = p.getX()+a; //Tehdään muunnos äksälle
		double y4 = p.getY()+b;
		
		p.setX(x4);	 //Laitetaan tämän olion uusi x arvo kohilleen metodin avulla (Koska arvo ei ole staattinen)
		p.setY(y4);
		
		return p; //palautetaan olio funktiolle
	}
	public static <T, R> Piste makeSkaalaus(double a, Piste p){
		
		p.setX(p.getX() * a);		
		p.setY(p.getY() * a);
		
		return p;
	}
	public static <T, R> Piste makeKierto(double a, Piste p){
		
		p.setX(p.getX() * Math.cos(a) - p.getY()*Math.sin(a));		
		p.setY(p.getX() * Math.sin(a) + p.getY()*Math.cos(a));
		
		return p;		
	}
	/*Tehdään getterit ja setterit x ja ylle, jotta niitä voidaan
	käyttää staattisissa metodeissa*/
	public double getX(){
		return this.x;
	}
	public double getY(){
		return this.y;
	}
	public void setX(double x){
		this.x = x;
	}
	public void setY(double y){
		this.y = y;
	}
	
}