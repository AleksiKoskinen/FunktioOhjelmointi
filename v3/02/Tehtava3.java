package V0302;

import java.util.function.IntSupplier;
import java.util.stream.IntStream;

public class Tehtava3 {

	public static void main(String [] args) {
		
		
		//luodaan anonyymin sisäluokan ilmentymä intS, jolla on arvo välil 1-39
		IntSupplier intS = new IntSupplier() {
		      public int getAsInt() {
		        return (int)(Math.random()*39+1);
		      }
		};
		
		/*
		  Tuotetaan IntStreamit joille generoidaan randomi numero 1-39 väliltä
	      ja rajoitetaan se tehtäväksi 7 kertaa
		*/
		IntStream.generate(() -> (int)(Math.random()*39+1)).limit(7).forEach(x -> System.out.print(x+", "));  //Lambda tyyliin
	 
		System.out.println("\n--------------------------");
		
		IntStream.generate(intS).limit(7).forEach(x -> System.out.print(x+", ")); //Sisäluokan ilmentymä (intS)
		    
	}	
	
}
