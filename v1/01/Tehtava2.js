'use strict';


function syt(p, q) {
 
    if(q == 0)
        return p;
    else 
        return syt(q, (p%q));  //Katsoo rekursivisesti suurimman yhteisen tekijän luvulle q ja p%q eli moduluksen tulokselle
}

console.log(syt(1500,220)); //Tässä siis 1500%220 = 180, ja 220 ja 180 suurin yhteinen tekijä on 20, eli tulos 20.