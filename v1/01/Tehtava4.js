'use strict';


function potenssiKorotus(luku, potenssi) {
    
    if (potenssi === 1) {
        return luku;
    }
    return  luku * potenssiKorotus(luku, potenssi-1);
}

console.log(potenssiKorotus(3,5)); 
