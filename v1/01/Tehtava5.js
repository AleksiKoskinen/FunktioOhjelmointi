'use strict';

var list = [1,2,3,4,5,6,7,8,9,10];

function reverseList(lista) {
    
   if(lista.length < 2) { 
        return lista;
    } else {
         /*pop() poistaa listan viimeisen elementin, ja laittaa sen 'talteen' heeppiin. Concat yhdistää kaksi listaa, eli tulee lista missä on 
         alussa tuo kyseinen elementti, ja sen jälkeen loppu lista, eli ensimmäisen runin jälkeen siellä on 10,1,2,3,4,5,6,7,8,9 */
        return [lista.pop()].concat(reverseList(lista)); 
    }                                                    
}

console.log(reverseList(list)); 
