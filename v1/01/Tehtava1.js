'use strict';

function isItPalindrome(merkkijono) {
    
    if(merkkijono.length == 1 || merkkijono.length == 0)
        return true;
    else if(merkkijono.charAt(0) != merkkijono.charAt(merkkijono.length-1))
        return false;
    else {
        merkkijono = merkkijono.slice(1,merkkijono.length);  //Poistetaan ensimmäinen kirjain 
        merkkijono = merkkijono.slice(0,merkkijono.length-1)  //Poistetaan viimeinen kirjain
        
        return isItPalindrome(merkkijono);
    }
}

console.log(isItPalindrome('imaaaami'));