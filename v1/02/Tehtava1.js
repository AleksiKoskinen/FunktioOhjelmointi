'use strict'

const result = function () {
    return function (one,two) {  //Anonyymi funktio paluuarvona
		if(one > two)
		    return 1;
		else if (two > one)
		    return -1;
		else   //Pakko olla yhtäsuuria in this case
		    return 0;
	 }
    }(); //Huomaa funktion kutsu

let tulos = result(9,4);

console.log(tulos);