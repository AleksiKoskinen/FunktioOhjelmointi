'use strict';

function potenssiKorotus(luku, potenssi) {
    //Laitetaan 3. muuttujaan arvo luku, jotta potenssin korotus summa alkaa annetusta luvusta
    return potenssiHelper(luku, potenssi, luku);  
    
}

function potenssiHelper(luku, potenssi, summa){  //Tarvitaan kolmas muuttuja (summa), johon kerrytetään laskettua tulosta.
    
    if (potenssi === 1) {
        return summa;
    }
    //Käytetään häntärekursiota eli rekursiivinen kutsu on viimeinen asia joka tehdään.
    return potenssiHelper(luku, potenssi-1, luku*summa); 
    
}

console.log(potenssiKorotus(2,5)); 
