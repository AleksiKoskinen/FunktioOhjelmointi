 'use strict';
        
        let f, g;
        function foo() {  
          let x;  
          f = function() { 
              return ++x; 
              
          }; 
          g = function() { 
              return --x; 
              
          }; 
          x = 1;
          console.log('inside foo, call to f(): ' + f());
        }
        foo();  //tekee sulkeuman muuttujan x ja f(), g() välille
        console.log('call to g(): ' + g()); 
        console.log('call to f(): ' + f());  
     
//Tulostetaan ensin foo funktion sisällä f()- Tässä f() on näkyvä koska on tapahtuu foo()'n sisällä.
//kun tehdään foo() kutsu, tekee f ja g funktio sulkeuman. kutsun jälkeen x häviää mutta jää heeppiin. f ja g'n sulkeumalla on siihen tällöin pääsy.
//tällöin kun tehdään esim g(), pääsee se käsiksi heepissä olevaan x'ään, jonka arvo on 2, ja miinustaa siitä yhden saaden tulokseksi 1.