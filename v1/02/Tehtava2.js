'use strict';

const hki2015 = [1,2,3,4,5,6,7,8,9,10];
const hki2016 = [4,5,6,7,8,1,2,3,4,22];

const result = function () {
    return function (one,two) {  //Anonyymi funktio
		if(one > two)
		    return 1;
		else if (two > one)
		    return -1;
		else   //Pakko olla yhtäsuuria in this case
		    return 0;
	 };
    }(); // Huomaa funktion kutsu!

function countHowMany(someFunction, hki2015,hki2016){
    
    let times = 0;
    
    //Lasketaan monessako kuukaudessa 2016 lämpötila > 2015 lämpötila
    for(var x = 0; x < hki2015.length; x++){
     if(someFunction(hki2016[x],hki2015[x]) == 1)
         times++;
    }
    return times;
}

console.log(countHowMany(result,hki2015,hki2016)); //Annetaan parametrinä anonyymi funktio, ja kaksi taulukkoa