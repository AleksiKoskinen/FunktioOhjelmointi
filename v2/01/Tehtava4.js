const hki2015 = [-1,2,3,4,5,6,7,8,9,-10,-5,-29];
const hki2016 = [-4,5,6,7,8,1,2,3,4,-22,-10,-24];

var countAverage = function(item,index){
    //otetaan molempien taulukoiden sama index paikka, ja jaetaan se 2, jolloin saadaan keskiarvo siitä kuukaudesta
    return (item + hki2016[index]) / 2;  
};

 //Tehdään uusi taulukko
var newHKI = hki2015.map(countAverage)  //Lasketaan keskiarvot kaikista hki2015 ja hki2016 taulukoista
            .filter(x => x > 0);  //filteröidään saadun uuden taulukon tuloksista negatiivisyys pois
            
console.log(newHKI);   //prints [ 3.5, 4.5, 5.5, 6.5, 3.5, 4.5, 5.5, 6.5 ]  , sum of 40, so 40/8 is 5

console.log(newHKI.reduce((accumulator,currentValue) => 
            accumulator + currentValue,  0 ) / newHKI.length );  //Saatu accumulator tulos pitää vielä jakaa taulukon pituudella, jotta saadaan keskiarvo
            