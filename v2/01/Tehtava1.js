var celsiusAste = ((fahrenheit) => {  //Tehdään nuolifunktio, eli tehdään celsiusAsteesta funktio
    return (5/9) * (fahrenheit-32);
    }
);  

var areaArvo = ((radius) =>  //Tehdään nuolifunktio, eli tehdään areaArvosta funktio.
    Math.PI * radius * radius  //sama idea kuin celsiusaste määrittelyssä, toteutettu vain ilman return lausetta eli erillailla.
    
); 

console.log(celsiusAste(100));
console.log(areaArvo(1));