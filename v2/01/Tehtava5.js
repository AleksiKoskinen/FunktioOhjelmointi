var fs = require('fs');

//Luetaan stringi txt tiedostosta, laitetaan tulos data muuttujaan
fs.readFile('kalevala.txt', 'utf8', function(err, data) {  
    if (err) throw err;


    var pattern = /(.+?)([\s,.!?;:)([\]]+)/g; //Tämä regex katsoo unicode kirjaimet myös, eli etsii jokaisen sanan ottaen huomioon ä ja ö't myös
 //   var pattern = /\w+/g; // / ja / ovat aloitus ja lopetusmerkit. \w+ etsii jokaista sanaa tarkoittavat jutskat. g (globaali) lopussa tarkoittaa että se ei lopeta ensimmäiseen löytöön
    var matchedWords = data.match( pattern ); //Match katsoo jokaisen sanan erikseen, joka pattern mahdollistaa, ja laittaa ne sanat putkeen 
    
    var counts = matchedWords.reduce(function ( stats, word ) {

        //stats on tässä se kertynyt objecti, word on matchedWords taulukosta yksittäinen sana
        
        if ( stats.hasOwnProperty(word) ) {  //Katsotaan onko stats objectilla jo olemassa haettu sana
            stats[word] = stats[word] + 1; //jos on, nin kasvatetaan sen arvoa yhdellä
        } else {  //jos stats objectilla ei ole vielä haettua sanaa
            stats[word] = 1; //Laitetaan sanan määräksi 1
        }
        return stats; //Palautetaan stats objecti seuraavalla iteraatiolle

    }, []);  //Alustetaan stats arraylla
    
    
    console.log(sortObjectKeys(counts));   
});  
    
/* Sorttaamiseen käytettävä funktio. Ensin siinä Object.keys(obj) antaa listan avaimista jotka ovat obj
objektissa. Sitten siihen tehdään sort() joka pistää ne aakkosjärjestykseen. Nytten tulos on array'nä, joten
halutaan se takas object muotoon. Tehdään reduce() alkoritmi, joka tekee arraystä jälleen objectin, mutta tällä kertaa
itse array on aakkosjärjestyksessä. */
       
function sortObjectKeys(obj){
     return Object.keys(obj).sort().reduce((acc,key)=>{
        acc[key]=obj[key];
        return acc;
    },{});
}








