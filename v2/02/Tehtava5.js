const Immutable = require('immutable');

var value = 0;

function add (){
    value += 2;
    return value;
}

/*esimerkkinä voidaan esim täyttää taulukon (Seq muodossa) 20 ekaa alkiota 
  sillain, että jokaisen alkion arvo kasvaa aina kahdella edellisestä.
  Jos siitä nyt jotain hyötyä on*/
  
var test1 = Immutable.Repeat().map( n => add() ).take( 20 );
console.log(test1);

//voidaan myös tulostaa tietty objecti x-kertaa, tässä tulostetaan sana palikka 4 kertaa
console.log(Immutable.Repeat('palikka',4));

//tässä tulostetaan sana pataljoona kertaa ääretön, koska siinä ei ole määritetty kertamäärää
console.log(Immutable.Repeat('pataljoona'));
     
