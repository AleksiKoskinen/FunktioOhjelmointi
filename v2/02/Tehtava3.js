const Immutable = require('immutable');

const set1 = Immutable.Set(['punainen', 'vihreä', 'keltainen']);
const set2 = set1.add('ruskea'); //Tekee uuden setin, joka sisältää ruskean

console.log(set1);  
console.log(set2);  

/*Huomataan, että set2 on saanut ruskea arvon muiden lisäksi, mutta set1 on edelleen sama,
ilman ruskea alkiota (immutable). Eli ne eivät ole samat*/
if(set1 === set2)
    console.log('Samat On');
else
    console.log('Eivät ole samat');

/*tehdään kolmas setti, jonka pitäisi saada taas uusi alkio nimeltä ruskea, mutta
koska set rakenteessa duplikaatit ovat kiellettyjä, niin se ei lisää sitä.
eli set3 ei tule olemaan: punainen, vihreä, keltainen, ruskea, ruskea
vaan se on sama kuin set2 */
const set3 = set2.add('ruskea');

console.log(set2);
console.log(set3);

/*rakenteet ovat samat, koska duplikaatteja ei lisätty*/
if(set2 === set3)
    console.log('Samat On');
else
    console.log('Eivät ole samat');

