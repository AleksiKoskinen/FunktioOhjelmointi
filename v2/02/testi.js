const Immutable = require('immutable');




// Seq
// lazy evaluated
//console.log('Seq');
//let multi = 2;
//const sequence = Immutable.Seq.of(1,2,3).map((value) => { return multi * value; }); // map function is not evaluated
//console.log(sequence);
//multi = 4;
//console.log(sequence.toJS()); // map function is evaluated


const results = Immutable.Seq.of(1, 2, 3, 4, 5)
                  .filter(x => x % 2)
                  .map(x => `odd: ${x}`);
                


 console.log(results.get(1));