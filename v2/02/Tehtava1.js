'use strict';
const normaaliKPiste = 85;
const suurmäenKPiste = 115;
const normaaliExtraPoint = 2;
const suurExtraPoint = 1.8;

//käytetään currying menetelmää
function laskuri(kPiste,lisaPisteet){
    return function(pisteet){
        //60 on kPisteelle päästyn hypyn pistemäärä. Voisi vielä tehdä extra funktion missä kaikki 3 parametria tähän returniin
        return 60 + ((pisteet - kPiste) * lisaPisteet); //palauttaa oikeat pisteet
    }; 
}

const normaaliLahti = laskuri(normaaliKPiste, normaaliExtraPoint);
const suurmäenLasku = laskuri(suurmäenKPiste, suurExtraPoint);

console.log(normaaliLahti(91));
console.log(suurmäenLasku(111));



