'use strict';

const Auto  = (function(){
    
    const matkaMittari = new WeakMap();  // yhteinen ilmentymille
    
    //kaikki ilmentymät käyttää nyt samaa tankin arvoa, koska muuttuja ei ole suojattu
    var tankki = 0;  
    
    class Auto{
        constructor(){
            matkaMittari.set(this, {lukema: 0}); //suojattu lukema arvo
        } 
        
        getMatkaLukema() {
            return matkaMittari.get(this).lukema;
        }
        getFuel(){
            return tankki;
        }
        addFuel(amount){
            tankki += amount;
        }
        aja(){
            matkaMittari.set(this,{lukema: matkaMittari.get(this).lukema+10});
            tankki -= 10;
        }
    }
    
    return Auto;
})();

var ferrari = new Auto();

ferrari.addFuel(100);
console.log("Ferrari matkaReadings: "+ferrari.getMatkaLukema());
console.log("Ferrari current fuel: "+ferrari.getFuel());
ferrari.aja();
console.log("Ferrari matkaReadings: "+ferrari.getMatkaLukema());
console.log("Ferrari current fuel: "+ferrari.getFuel());

var porsche = new Auto();

//Huomataan alla olevasta tuloksustesta, että porchen alku matkalukema on 0, mutta
//fuel on pysynyt ferrarin lukemassa, eli 90. Koska niillä on yhteinen suojaamaton muuttuja
console.log("Porsche matkaReadings: "+porsche.getMatkaLukema());
console.log("Porsche current fuel: "+porsche.getFuel());
porsche.aja();
console.log("Porsche matkaReadings: "+porsche.getMatkaLukema());
console.log("Porsche current fuel: "+porsche.getFuel());