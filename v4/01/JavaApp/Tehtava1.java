package V0401;

import org.junit.Test;

import static org.junit.Assert.*;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class Tehtava1 {

	
	@Test
	public void testmoveAllPointsRightBy() throws Exception {
		
		List<Point> points = Arrays.asList(new
		Point(5,5), new Point(10,5));
		List<Point> expectedPoints =
		Arrays.asList(new Point(15,5),
		new Point(20,5));
		List<Point> newPoints = Point.
		moveAllPointsRightBy(points, 10);
		
		for(int i = 0; i<expectedPoints.size();i++) {
			assertEquals(expectedPoints.get(i).getX(), newPoints.get(i).getX());
		}
	}
	
		
	private static class Point{
        private int x;
        private int y;

        private Point(int x, int y) {
            this.x = x;
            this.y = y;
        }

        public int getX() {
            return x;
        }
        
        public Point moveRightBy(int x) {
            return new Point(this.x + x, this.y);
        }
        public static List<Point> moveAllPointsRightBy(List<Point> list, int points) {
        	
        	return list.stream().map(p -> p.moveRightBy(points)).collect(Collectors.toList());
        }
    }
}

/*
 * 1. Debugging.java-tiedostossa on Point-luokan toteutus. Luennolla esitettiin moveAllPointsRightBy-metodi, 
jolla saadaan siirrettyä kaikki listan pisteet oikealle. 
Toteuta tämä loppuun asti niin, että myös testit menevät läpi. 

Vinkki. AssertArrayEquals() käyttää equals()-metodia. Miten varmistetaan pisteiden yhtäsuuruus?
*/
