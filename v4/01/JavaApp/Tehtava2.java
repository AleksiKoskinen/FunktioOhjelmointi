package V0401;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.function.Function;
import java.util.stream.IntStream;

public class Tehtava2 {

	public static void main(String args[]) {
		
		List<Integer> taulukkoLista = new ArrayList<Integer>();
		LinkedList<Integer> linkitettyLista = new LinkedList<Integer>();
		
		for(int i = 0; i < 10000000; i ++){
			taulukkoLista.add(i);
			linkitettyLista.add(i);
		}
		IntStream intStream = IntStream.iterate(0, i -> i + 1).limit(10000000); 
		
		System.out.println("Array list sum done in: " + doAction(Streams::hidasNelioSumma, taulukkoLista) + " msecs");
		System.out.println("Linked list sum done in: " + doAction(Streams::hidasNelioSummaLinked, linkitettyLista) + " msecs");
		System.out.println("IntStream list sum done in: " + doAction(Streams::hidasNelioSummaIntStream, intStream) + " msecs");
				
	}
	
	public static <T,R> long doAction(Function<T,R> f, T input) {
		long fastest = Long.MAX_VALUE;
		
		for (int i = 0; i < 10; i++) {
            long start = System.nanoTime();
            R result = f.apply(input);
            long duration = (System.nanoTime() - start) / 1_000_000;
            System.out.println("Result: " + result);
            if (duration < fastest) fastest = duration;
        }
        return fastest;
	}
		
}

class Streams {

    public static int hidasNelioSumma(List<Integer> list) {
        return list.parallelStream()
                                  .map(x -> x * x)
                                  .reduce(0, (acc, x) -> acc + x);
    }
    public static int hidasNelioSummaLinked(LinkedList<Integer> list) {
        return list.parallelStream()
                                  .map(x -> x * x)
                                  .reduce(0, (acc, x) -> acc + x);
    }
    public static int hidasNelioSummaIntStream(IntStream list) {
        return IntStream.range(1, 10000000).map(i -> i * i)
                                  .reduce(0, (acc, x) -> acc + x);
    }    
    
}

/*
2. Seuraava ratkaisu on melko hidas, vaikka käytetäänkin rinnakkaista vuota. 
Tee samanlainen vertailu kuin ParallelStreamsHarness.java-ohjelmassa erilaisista toteutustavoista. Tee erilaisia ratkaisuja, joissa
- käytetään linkitettyä listaa
- taulukkolistaa
- suoraan IntStreamia

Alusta listat ja vuot riittävän isolla alkioiden määrällä niin, että ajoissa saat eroja.

```
taulukkoLista = new ArrayList<>();
linkitettyLista = new LinkedList<>();

public int hidasNelioSumma() {
        return linkitettyLista.parallelStream()
                                  .map(x -> x * x)
                                  .reduce(0, (acc, x) -> acc + x);
}
```
*/