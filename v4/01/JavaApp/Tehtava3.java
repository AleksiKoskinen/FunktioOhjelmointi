package V0401;

import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class Tehtava3 {

	public static void main(String...args) throws Exception{
	       
        //Kuten esimerkissä, haetaan tiedosto sijainnista, käytetään splittiä erottamaan sanat välilyönnillä
   	 //, flatmapilla viedään jokainen sana mappiin, ja lopuksi tehdään siitä String lista
		
		long start = System.nanoTime();
        List<String> list = Files.lines(Paths.get("C:/Users/Aleksi/Desktop/Java8Funktio/Java8/src/V0301/kalevala.txt"), Charset.defaultCharset())
                                .parallel() //Tehdään rinnakkaisuus eli parallel()
        						.flatMap(line -> Arrays.stream(line.split(" ")))
                                .collect(Collectors.toList());
  
        //Tehdään String, integer pari Map, johon laitetaan listan streemistä ensin itse sana Stringiksi
        //, ja sitten sen arvo laitetaan 1, jos se on jo siellä, niin Integer::sum plussaa vanhan arvon ja ykkösen,
        //eli kasvattaa yhdellä sanan arvoa
        Map <String, Integer> count = list.stream().parallel() //Tehdän rinnkkaisuus eli parallel()
            .collect(Collectors.toMap(w -> w, w -> 1, Integer::sum));
        
        //Katsotaan kauan suoritukseen kesti, kun on tehty rinnakkaisuus eli parallel()
        long duration = (System.nanoTime() - start) / 1_000_000;
        
        System.out.println(duration+ " msecs");
        System.out.println(count);
       
   }
}

/*
3. Pystyisitkö rinnakkaistamaan viime viikon ratkaisuasi, joka tekee esiintymälistan kalevala.txt-tiedossa
esiintyvistä sanoista? Vertaa suoritusaikoja eri ratkaisuilla.

*/

