package V0402;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.function.BiPredicate;
import java.util.function.Consumer;
import java.util.function.IntSupplier;
import java.util.function.Supplier;

public class Tehtava9 {

	public static void main(String[] args){
	    
		NumberGame game = new NumberGame();
        
		//Pelataan peli kolmella pelaajalla
		game.playOneGame(3);
        
   }
}

class NumberGuessTemplateMethod {

    public final void handleGame(
        IntSupplier playerCount, //Otetaan pelaajien määrä
        Supplier<List<Pelaaja>> pelaajat, //Alustetaan pelaajalista
        Supplier<Peli> initializeGame, //Alustetaan peli olio
        BiPredicate<List<Pelaaja>, Peli> endOfGame, //Katsotaan onko peli päättynyt
        Consumer<Pelaaja> makePlay, //Tehdään siirto valitulla pelaajalla
        Consumer<Peli> printWinner) { //Tulostetaan voittajan fanfaaari

        List<Pelaaja> p;
        int players;
        Peli game;
        
        players = playerCount.getAsInt(); //Otetaan pelaajien lkm
        p = pelaajat.get(); //Alustetaan lista
        game = initializeGame.get(); //Alustetaan peli
        
        int j = 0;
        
        while (!endOfGame.test(p , game)){   //Katsotaan onko voittajaa
            makePlay.accept(p.get(j));  //Pelaaja tekee arvauksen
            j = (j + 1) % players;
        }
        printWinner.accept(game);  //Tulostetaan voittaja
    }
    
}

class NumberGame {
    
    Scanner scan = new Scanner(System.in);
    
    public void playOneGame(int playersCount){
                
        new NumberGuessTemplateMethod().handleGame(
                () -> playersCount,
                () -> {     
            		List<Pelaaja> p = new ArrayList<Pelaaja>();
                    for(int i = 0; i < playersCount; i++){
                    	p.add(new Pelaaja(i+1)); //Lisätään pelaajat 
                    }
                    return p;
                	},
                () -> new Peli(5) //Tehdään uusi peli, jonka voittaa numerolla 5
                ,
                (List<Pelaaja> p, Peli game) -> {
                	//Katsotaan pelaajat läpi, että onko joku arvannut oikein
                	for(int i = 0; i < playersCount; i++){
                		if (p.get(i).getGuess() == game.getNumber() ){
                			//Laitetaan peli olioon voittajan numero
                			game.setGameWinner(p.get(i).getNumber());
                			return true;
                		}
                	}
                	return false;
                },
                (Pelaaja p) -> {
                	System.out.println("Pelaaja "+ p.getNumber() + ". Syötä numero: ");
                    int numero = scan.nextInt();
                    p.setGuess(numero);//Laitetaan pelaajan uusi arvaus
                },
                (Peli game) -> System.out.println("Pelaaja numero "+ game.getGameWinner() + " Voitti Pelin!")
        );        
    }   
}

class Pelaaja {
    
	//Pelaajan numero ja pelaajan arvaus
    private int number, guessedNumber;
       
    public Pelaaja(int numero) {
        this.number = numero;
    }
    public int getNumber(){
    	return number;
    }
    public void setGuess(int g){
    	this.guessedNumber = g;
    }
    public int getGuess(){
    	return this.guessedNumber;
    }
}
class Peli{
	
	//Winningnumber on numero, jolla pelin voi voittaa, winner on voittavan pelaajan numero
	private int winningNumber, winner;
	
	public Peli(int wnumber){
		this.winningNumber = wnumber;
	}
	public int getNumber(){
		return winningNumber;
	}
	public void setGameWinner(int winner){
		this.winner = winner;
	}
	public int getGameWinner(){
		return winner;
	}
}


/*
 * Vuoropohjaisen pelin (shakki, pokeri jne. ) logiikka voidaan esittää seuraavalla algoritmilla (= Template Method):

```
 abstract class Game {
 
    protected int playersCount;
    abstract void initializeGame();
    abstract void makePlay(int player);
    abstract boolean endOfGame();
    abstract void printWinner();

    public final void playOneGame(int playersCount) {
        this.playersCount = playersCount;
        initializeGame();
        int j = 0;
        while (!endOfGame()){
            makePlay(j);
            j = (j + 1) % playersCount;
        }
        printWinner();
    }
```
* Pystytkö esittämään tälle funktionaalisen version ja jonkin pelin toteutuksen? 
* Katso mallia gitistä v4/02/TemplateMethod.
* */
