package V0402;

import static java.util.stream.Collector.Characteristics.*;

import java.util.ArrayList;
import java.util.Collections;
import java.util.EnumSet;
import java.util.List;
import java.util.Set;
import java.util.function.BiConsumer;
import java.util.function.BinaryOperator;
import java.util.function.Function;
import java.util.function.Supplier;
import java.util.stream.Collector;
import java.util.stream.Stream;

public class Tehtava1 {

	public static void main(String[] args) {
	    Stream<String> s = Stream.of("a","b","c","a","b","c","a","b","c","a","b","c","a","b","c","a","b","c","a","b","c","a","b","c","a","b","c","a","b","c","a","b","c","a","b","c","a","b","c","a","b","c","a","b","c","a","b","c","a","b","c","a","b","c","a","b","c","a","b","c","a","b","c","a","b","c","a","b","c","a","b","c","a","b","c","a","b","c","a","b","c","a","b","c","a","b","c","a","b","c","a","b","c");
	    
	    StringBuffer names = s
	    	.parallel()
	        .collect(new OmaJoiningKollektori<>()); 

	    System.out.println(names);
	    
	    Omena o1 = new Omena("vihreÃ¤", 100);
        Omena o2 = new Omena("vihreÃ¤", 110);
        Omena o3 = new Omena("punainen", 101);
        Omena o4 = new Omena("punainen", 105);
        
        List<Omena> omenat = new ArrayList<>();
        
        omenat.add(o1);
        omenat.add(o2);
        omenat.add(o3);
        omenat.add(o4);
        
        StringBuffer omenia = 
                omenat.stream()
                	.parallel()
                    .collect(new OmaJoiningKollektori<>()); 
                
        
        System.out.println(omenia);
	}
	
}

class OmaJoiningKollektori<T> implements Collector<T, StringBuffer, StringBuffer> {

    @Override
    public Supplier<StringBuffer> supplier() {    
    	System.out.println("HERE 1");
        return () -> new StringBuffer();
    }


    @Override
    public BiConsumer<StringBuffer, T> accumulator() {  
    	System.out.println("HERE 2");
        return (list, item) -> list.append(item);
    }

    @Override
    public BinaryOperator<StringBuffer> combiner() {   
    	System.out.println("HERE 3");
        return (list1, list2) -> {
            System.out.println("c: "+list1.toString()+" list2: "+list2.toString());
            list1.append(list2);
            System.out.println("HERE 4");
            return list1;
        };
    }

    @Override
    public Function<StringBuffer, StringBuffer> finisher() {
    	System.out.println("HERE 5");
        return Function.identity();
    }

    @Override
    public Set<Characteristics> characteristics() {
    	System.out.println("HERE 6");
        return Collections.unmodifiableSet(EnumSet.of(IDENTITY_FINISH, CONCURRENT ));
    }  //UNORDERED
}

/*1. a) Kirjoita oma Collector-rajapinnan toteuttaja, joka toimii kuten Collector.joining()-tehdasmetodin palauttama Collector-toteutus.
Tuota koodaamasi kollektorin avulla merkkijono ArrayList-tietorakenteen sisältämistä Omena-olioista. 
Kirjoita myös oma joining()-metodia vastaava staattinen tehdasmetodi omaan OmatKollektorit-luokkaan ja käytä sitä.
* Vinkki: käytä StringBufferia Supplierin paluuarvona.
<br><br>
b) Pystyykö edellisen toteuttamaan yhdellä usean säikeen yhteisellä StringBufferilla, jos hyödynnetään rinnakkaisuutta?
Testaa sopivalla, riittävän isolla aineistolla.
* Testaa siis CONCURRENT -optimoinnin käyttö. Järjestyksellä ei ole väliä tuloslistaa muodostettaessa.
* -->tee se supplier, biConsume ja finisher()
*/
