package V0402;

import java.text.Normalizer;

public class Tehtava7 {

	public static void main(String[] args){	
	      
		//Laitetaan Strategia konstruktorissa, heti laitetaan mitä strategia tekee
	    StrategyContext konteksti = new StrategyContext((String s) -> s.replaceAll("\\s+"," "));
	    //Syötetään data ja tehdään strategialla muunnos siihen    
	    konteksti.setData("This is a    test document  for the exercise. sturct , ä å , ö, Ä Å , Ö, sturct again");
	    konteksti.modifyData();
	    System.out.println("Spaces removed: " +konteksti.getData());
	    //Vaihetaan strategiaa, eli setStrategylla suoraan vaihetaan se, minkälaisen muunnoksen strategia tekee
	    konteksti.setStrategy((String s) ->  s = Normalizer.normalize(s, Normalizer.Form.NFD).replaceAll("[^\\x00-\\x7F]", ""));
	    konteksti.modifyData();
	    System.out.println("UNICODE removed: " +konteksti.getData());    
	    
	    konteksti.setStrategy((String s) -> s.replaceAll("\\bsturct\\b", "struct"));
	    konteksti.modifyData();
	    System.out.println("Word fixed: " +konteksti.getData());     
	}
		
}

@FunctionalInterface
interface Strategy{
     String apply(String s);
}

class StrategyContext {
        
	private Strategy strategy;
    private String data;
    
    public StrategyContext(Strategy strategy) {
        this.strategy = strategy;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }
    
    public void modifyData(){
        data = strategy.apply(data);
    }
    public void setStrategy(Strategy strategy){
    	this.strategy = strategy;
    }
   
    
    
}

/*
7. Edellisessä tehtävässä tehdyt toiminnallisuudet voidaan nähdä merkkijonon käsittelyyn liittyvinä erilaisina editointistrategioina. 
Tarkastele asiaa tästä näkökulmasta ja toteuta merkkijonon käsittely Strategy-mallin mukaisesti. 
* Katso mallia gitistä v4/02/Strategy.
*/