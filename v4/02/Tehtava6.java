package V0402;

import java.text.Normalizer;
import java.util.function.Function;
import java.util.function.UnaryOperator;

public class Tehtava6 {

	public static void main(String args[]){
		
		UnaryOperator<Document> deleteExtraSpaces = (Document doc) -> 
		//\s regex tarkoittaa space charakterii. + perässä tarkoittaa että voi olla monta spacee putkeen
                    {   doc.setContext(doc.getContext().replaceAll("\\s+"," "));
                        return doc;
                    };
        
        UnaryOperator<Document> fixUnicode = (Document doc) -> 
                    {   String s = doc.getContext();
                    /*Käytetään java.text pakkauksen Normalizer luokkaa, joka muuttaa tekstin sellaiseen muotoon
                      että siitä voi muuttaa UNICODE merkit vastaavanlaisiksi. */
                       	s = Normalizer.normalize(s, Normalizer.Form.NFD);
                    //Laitetaan regex, joka etsii ASCII kirjaimet ja korvaa ne normalisoituun muotoon (Form)
                    	doc.setContext(s.replaceAll("[^\\x00-\\x7F]", ""));
                        return doc;
                    };
        UnaryOperator<Document> fixWord = (Document doc) -> 
        //Jälleen regexiä käytetään. tehdään sanan ympärille boundariet eli 'b'. Tällöin jokainen sturct sana korvataan
                    {   doc.setContext(doc.getContext().replaceAll("\\bsturct\\b", "struct"));                    	
                        return doc;
                    };
        
        Function<Document, Document> ketju = deleteExtraSpaces.andThen(fixUnicode).andThen(fixWord); 
        
        Document doc = new Document("This is a    test document  for the exercise. sturct , ä å , ö, Ä Å , Ö, sturct again");
        
        doc = ketju.apply(doc);
        
        System.out.println(doc.getContext());
	}
}

class Document {

    private String context;
    
    public Document(String ct) {
        this.context = ct;
    }

    public void setContext(String ct) {
        this.context = ct;
    }  
    public String getContext() {
        return this.context;
    }  
   
}

/*
6. Määrittele vastuuketju (Chain of Responsibility), jossa käsitellään merkkijonona välitettävä dokumentti.
Ensimmäinen käsittelijä poistaa dokumentista kaikki ylimääräiset välilyönnit. Toinen korvaa skandit seuraavasti: ä,å -> a; ö -> o; Ä,Å -> A; Ö -> O. 
Kolmas tarkistaa oikeinkirjoituksen: kaikki sturct-sanat korvataan struct-sanoilla.
* Katso mallia gitistä v4/02/ChainOfResponsibility.

*/