package V0402;

import static java.lang.Thread.sleep;
import java.util.HashMap;
import java.util.Map;
import java.util.Observable;

public class Tehtava8 {

	static String lastHKI = "", lastIlta = "";
	
	public static void main(String[] args) {
	     
		Uutistoimisto s1 = new Uutistoimisto();
                
		StringBuilder Sanoma = new StringBuilder(); 
        StringBuilder Ilta = new StringBuilder(); 
        
        
        s1.addObserver((Observable o, Object arg) -> {      
        	
        	//tehdään objekti argista MAP tyyppinen
        	Map<String,String> HKISanoma = (HashMap) arg;
        	/*Alla katsotaan, että jos yritetään lisätä sama uutinen, kuin juuri aikaisemmin, niin 
        	 * se huomataan eikä lisätä duplikaatti uutisia. Eli lastHKI muuttuja ottaa aina talteen juuri lisätyn uutisen,
        	 * ja kun uutista yritetään lisätä uudestaan, niin jos sen arvo on sama kuin lisättävän uutisen, ei sitä silloin lisätä
        	 */
        	if(!lastHKI.equals(HKISanoma.get("Politiikka"))){
        		Sanoma.append(HKISanoma.get("Politiikka")+ ". ");
        		lastHKI = HKISanoma.get("Politiikka");
        	}
        		          	
        });
        
        s1.addObserver((Observable o, Object arg) -> {
        	Map<String,String> IltaSanoma = (HashMap) arg;     
        	if(!lastIlta.equals(IltaSanoma.get("Julkkis"))){
        		Ilta.append(IltaSanoma.get("Julkkis")+ ". ");  		
        		lastIlta = IltaSanoma.get("Julkkis");
        	}
        	
        });
        
        Thread t1 = new Thread(s1);
        t1.start();

           
        try {
            t1.join();
        } catch (InterruptedException ex) {
        }
        
        System.out.println(Sanoma.toString());
        System.out.println(Ilta.toString());
        
    }
	
}

class Uutistoimisto extends Observable implements Runnable{

	HashMap<String,String> uutinen = new HashMap<String,String>();
	
    @Override
    public void run() {
    	try {
	    	uutinen.put("Politiikka", "Donaldin liito on ennätysmäisissä lukemissa!");
	    	uutinen.put("Julkkis", "Cheek lopettaa uransa, vihdoinkin");
	    	setChanged();
            notifyObservers(uutinen);
            
	    	uutinen.put("Politiikka", "Kimin ydinase poksahti hänen linnaansa");
	    	uutinen.put("Julkkis", "Sillanpää on uusin hörhöpää.");
	    	setChanged();
            notifyObservers(uutinen);
             
            /* Testataan että samaa uutista ei voi lisätä uudestaaan
             * Eli alla olevat observerin notifikaatiot eivät tee mitään,
             * koska se on metodissa estetty
             */
            uutinen.put("Politiikka", "Kimin ydinase poksahti hänen linnaansa");
	    	uutinen.put("Julkkis", "Sillanpää on uusin hörhöpää.");
	    	setChanged();
            notifyObservers(uutinen);
            sleep(100);
    	} catch (InterruptedException ex) {}
         
    }    
}


/*
8. Uutistoimisto tuottaa uutisia, joita se välittää eri medioille (Helsingin Sanomat, Ilta-Sanomat jne.) 
Kirjoita ja testaa Observer-mallin mukainen ratkaisu, 
jossa Helsingin Sanomat välittää lukijoilleen (tulostaa ruudulle) kaikki uutiset, joista löytyy avainsana 
"politiikka" ja Ilta-Sanomat jokaisen, josta löytyy avainsana "julkkis".

* Katso mallia gitistä v4/02/Observer.
*/