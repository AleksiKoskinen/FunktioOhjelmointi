package V0402;

import static java.util.stream.Collector.Characteristics.*;

import java.util.ArrayList;
import java.util.Collections;
import java.util.EnumSet;
import java.util.List;
import java.util.Set;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.function.BiConsumer;
import java.util.function.BinaryOperator;
import java.util.function.Function;
import java.util.function.Supplier;
import java.util.stream.Collector;
import java.util.stream.DoubleStream;
import java.util.stream.Collector.Characteristics;

/*Kun tehdään parallel striimeihin, eli rinnaistetaan, niin huomataan
 * että kollektorissa käydään eka characteristissa katsomassa tyyli mitä käytetään, sitten mennään
 * eka tekemään supplier eli tyhjä lista, siitä mennää BiConsumeriin lisäämään listaan kaikki alkiot
 * , ja sitten combineriin  on kahdesta alkiosta tullut 2 eri listaa. Ne yhdistetään yhdeksi listaksi,
 * jolloin saadaan lopputulos. 
 * Jos laittaa concurrend charasteriksiin, niin se näyttäisi tekevän vain 2 eri listaa 2 alkiosta, mutta
 * jos laittaa unordered niin näyttäisi tulevan paljon pieniä osia listoista, jotka sitten yhdistetään. 
 * Jos tehdään ilman rinnakkaisuutta niin combineriin ei koskaan mennä kun siihen ei ole tarvetta.
 */

public class Tehtava3 {

	public static void main(String[] args){
	    
        Omena o1 = new Omena("vihreÃ¤", 100);
        Omena o2 = new Omena("vihreÃ¤", 110);
        Omena o3 = new Omena("punainen", 101);
        Omena o4 = new Omena("punainen", 105);
        
        List<Omena> omenat = new ArrayList<>();
        
        omenat.add(o1);
        omenat.add(o2);
        omenat.add(o3);
        omenat.add(o4);
            
        
        List<Omena> punaiset = 
                omenat.stream()
             //   .parallel()
                    .filter(o -> "punainen".equals(o.getVari()))
                    .collect(new OmaListaKollektori<>()); 
                
        punaiset.forEach(o -> System.out.println(o));
        // punaiset.forEach(System.out::println)


        List<Omena> vihreat = 
                omenat.stream()
                .parallel()
                    .filter(o -> "vihreÃ¤".equals(o.getVari()))
                    .collect(new OmaListaKollektori<>()); 
                
        vihreat.forEach(o -> System.out.println(o));
      
  
        int cores = Runtime.getRuntime().availableProcessors();
        System.out.println("Cores: " + cores);
        
        long start = System.nanoTime();
        								
        								
        								
        List<Double> l = DoubleStream.generate(Math::random)        
        .limit(50000)        
        .boxed()
        .parallel()
        .collect(new OmaListaKollektori<>());  // ikioma kollektori
       // .collect(toList()); // Javan oma
       
        long duration = System.nanoTime() - start;
       
        System.out.println(l.size() + " " + duration / 1000000);
        
      
    } 
}

class OmaListaKollektori<T> implements Collector<T, List<T>, List<T>> {
/*    
    @Override
    public Supplier<List<T>> supplier() {
        return new Supplier<List<T>>(){
            @Override
            public List<T> get(){
                return new ArrayList<>();
            }
        };
    }

*/
    @Override
    public Supplier<List<T>> supplier() {     // () -> T
    	System.out.println("HERE 1");
        return () -> new CopyOnWriteArrayList<>();
    }


    @Override
    public BiConsumer<List<T>, T> accumulator() {  // T, U -> void
    	System.out.println("HERE 2");
        return (list, item) -> list.add(item);
    }

    @Override
    public BinaryOperator<List<T>> combiner() {   // T, T -> T
    	System.out.println("HERE 3");
        return (list1, list2) -> {
            System.out.println("c: "+list1.toString()+" list2: "+list2.toString());
            list1.addAll(list2);
            System.out.println("HERE 4");
            return list1;
        };
    }

    @Override
    public Function<List<T>, List<T>> finisher() {
        //return list -> list;  
    	System.out.println("HERE 5");
        return Function.identity();
    }

    @Override
    public Set<Characteristics> characteristics() {
    	System.out.println("HERE 6");
        return Collections.unmodifiableSet(EnumSet.of(IDENTITY_FINISH, CONCURRENT));
    }
}
class Omena{
    
    private String vari;
    private int paino;
    
    public Omena(String vari, int paino){
        this.vari = vari;
        this.paino = paino;
    }
    
    public int getPaino(){
        return paino;
    }
    
    public String getVari(){
        return vari;
    }
    
    
    public String toString(){
        return "Omena: " + vari + ":" + paino + " g";
    }
    
}
