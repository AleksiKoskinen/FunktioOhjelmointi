(ns tehtavat.core
  (:gen-class))

;Tehtava 1 below

(def muod (partial str "Muodollinen "))
(def emuod (partial str "Epamuodollinen "))
(def suo (partial str "Suomeksi "))

;core=> (suo "Aleksi")  => "Suomeksi Aleksi"

;Tehtava2 below

(def taulu [[1 2 3][4 5 6][7 8 9]])

;a)

(def sekki (map #(apply min %) taulu))

;core=> sekki => (1 4 7)
;b)

(def newvector (apply vector sekki))

;core=> newvector => [1 4 7]

;Tehtava3 below

(def omasima
     [{:aines "Vesi", :yksikko "litraa", :maara 4} 
     {:aines "Sokeri", :yksikko "grammaa", :maara 500} 
     {:aines "Sitruuna", :yksikko "kpl", :maara 2} 
     {:aines "Hiiva", :yksikko "grammaa", :maara 1}])

(defn kertaa
  [luku kerroin]
  (* luku kerroin))

;tehdään uusi seq, jossa on omasima taulukon määrä kerrottuna x (3) määrällä
(def newrecept (map #(update % :maara kertaa 3) omasima))

;core=> newrecept => ({:aines "Vesi", :yksikko "litraa", :maara 12} {:aines "Sokeri", :yksikko "grammaa", :maara 1500} {:aines "Sitruuna", :yksikko "kpl", :maara 6} {:aines "Hiiva", :yksikko "grammaa", :maara 3})

