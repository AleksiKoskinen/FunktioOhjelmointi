(ns tehtavat.core
  (:gen-class))

;Teht1 below

(def lampo2015 [5,3,4,-1,11,3,-12,5,2,4,-19,5]) ; 2015 lampotilat taulukossa
(def lampo2016 [5,6,7,-5,8,9,-15,1,2,3,-19,5]) ; 2016 lampotilat taulukossa

(def keskiWithPositives (filter #(> % 0) (map #(/ (+ % %2) 2) lampo2015 lampo2016))) ;Tehdään uusi taulukko jossa on vain positiiviset kahden taulukon keskiarvot

(defn countLampo []  ;tehdään funktio jota voi käyttää, se laskee taulukosta lukujen keskiarvon
    (/ (reduce + keskiWithPositives) (count keskiWithPositives)) ;lasketaan alkiot yhteen , ja jaetaan määrällä.
)

;Teht2 below

(def food-journal
  [{:kk 3 :paiva 1 :neste 5.3 :vesi 2.0}
   {:kk 3 :paiva 2 :neste 5.1 :vesi 3.0}
   {:kk 3 :paiva 13 :neste 4.9 :vesi 2.0}
   {:kk 4 :paiva 5 :neste 5.0 :vesi 2.0}
   {:kk 4 :paiva 10 :neste 4.2 :vesi 2.5}
   {:kk 4 :paiva 15 :neste 4.0 :vesi 2.8}
   {:kk 4 :paiva 29 :neste 3.7 :vesi 2.0}
   {:kk 4 :paiva 30 :neste 3.7 :vesi 1.0}])
 
(defn summa []
    (reduce #(+ %  (if (> (:kk %2) 3) ;%1 on 0, eli lähtöarvo, katsotaan joka alkiosta onko :kk 4, eli huhtikuu
                        (- (:neste %2) (:vesi %2)) ;jos on, niin vähennetään neste määrästä vesi, jolloin saadaan muun veden kulutus
                        0  ;ei lisätä mitään, jos ei olla huhtikuussa
                    )
             ) 0 food-journal ;lähtöarvo 0, käytetään %2 arvona taulukkoa
    )
)
;ja reducella saadaan lopputulos (10,3)


;Teht3 below

(defn food-journal2 [] ;tehdään funktio jolla voi replissä tulostaa taulukon 
    (mapv #(select-keys % [:kk :paiva :muuneste]);mapv tekee vektori mapin suoraan, anonyymi funktio, johon valitaan parametrina tehdystä taulukosta kk paiva muuneste vain. 
          (for [x (take-while #(> (:kk %) 3) ;tehdään taulukko, johon otetaan food journalista vain :kk > 3 arvot
                                             ;ja pudotetaan :kk < 4 arvot. eli 4 arvoisiin :kk lisätään :muuneste avain, jonka arvo tulee sen :kk'n :neste-:vesi
            (drop-while #(< (:kk %) 4) food-journal)  )] (conj x [:muuneste (- (:neste x)  (:vesi x))])))
)


